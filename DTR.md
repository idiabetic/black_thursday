### Template for DTR Memo
Project: Black Thursday
Group Member Names:  Dylan Patrick

Project Expectations: What does each group member hope to get out of this project?

Dylan: A better understanding of how to work with databases. Get better at testing

Patrick: A better of how to build an O.R.M.

Goals and expectations:

Finish the project and meet all the spec.

#### Team strengths:  

Problem solving and db experience.

#### How to overcome obstacles:

Open honest communication

#### Schedule Expectations (When are we available to work together and individually?):

Both of us are pretty open and flexible. Goal is to knockout the in person pairing
in the first weekend and then attempt remote work.

#### Communication Expectations (How and often will we communicate? How do we keep lines of communication open?):

Slack, github

#### Abilities Expectations (Technical strengths and areas for desired improvement):

Improving single responsibility, UI design, using git properly and working together.

#### Workload Expectations (What features do we each want to work on?):

We'll work on the first two iterations together and then build the remaining pieces somewhat seperately.

#### Workflow Expectations (Git workflow/Tools/Code Review/Reviewing Pull Requests):

Don't merge your own pull requests.
Ensure there aren't too many rubocop/hound issues.
Always work in a branch

#### Expectations for giving and receiving feedback:

Just be honest.

Agenda to discuss project launch:

Ideas:

#### Tools:
* Ruby 2.3.6
* Rubocop
* Hound CI
* SimpleCov

Additional Notes:
